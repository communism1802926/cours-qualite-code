const fs = require('fs');
const path = require('path');
const TennisGame1 = require('../src/TennisGame1');
const TennisGame1Refacto = require('../src/TennisGame1.refacto');

describe('Golden Master tests for TennisGame1', () => {
    const scenarios = [
        { actions: [], expected: "Love-All" },
        { actions: ["player1"], expected: "Fifteen-Love" },
        { actions: ["player1", "player2"], expected: "Fifteen-All" },
        { actions: ["player1", "player1"], expected: "Thirty-Love" },
        { actions: ["player1", "player1", "player2"], expected: "Thirty-Fifteen" },
        { actions: ["player1", "player1", "player2", "player2"], expected: "Thirty-All" },
        { actions: ["player1", "player1", "player2", "player2", "player1"], expected: "Forty-Thirty" },
        { actions: ["player1", "player1", "player2", "player2", "player1", "player2"], expected: "Deuce" },
        { actions: ["player1", "player1", "player2", "player2", "player1", "player2", "player1"], expected: "Advantage player1" },
        { actions: ["player1", "player1", "player2", "player2", "player1", "player2", "player1", "player2"], expected: "Deuce" },
        { actions: ["player1", "player1", "player2", "player2", "player1", "player2", "player1", "player2", "player1"], expected: "Advantage player1" },
        { actions: ["player1", "player1", "player2", "player2", "player1", "player2", "player1", "player2", "player1", "player1"], expected: "Win for player1" },
        { actions: ["player2", "player2", "player2", "player2"], expected: "Win for player2" },
        { actions: ["player2", "player2", "player2", "player2", "player1", "player1", "player1", "player1"], expected: "Deuce" },
        { actions: ["player2", "player2", "player2", "player2", "player1", "player1", "player1", "player1", "player2"], expected: "Advantage player2" },
        { actions: ["player2", "player2", "player2", "player2", "player1", "player1", "player1", "player1", "player2", "player2"], expected: "Win for player2" },
    ];

    const resultsOriginal = [];
    const resultsRefacto = [];

    // Generate results for original version
    scenarios.forEach(({ actions, expected }, index) => {
        const game = new TennisGame1("player1", "player2");
        actions.forEach(player => game.wonPoint(player));
        const score = game.getScore();
        resultsOriginal.push({ scenario: index + 1, actions, score, expected });
    });

    const outputPathOriginal = path.join(__dirname, 'TennisGame1GoldenMasterResults.json');
    fs.writeFileSync(outputPathOriginal, JSON.stringify(resultsOriginal, null, 2), 'utf-8');

    // Generate results for refactored version
    scenarios.forEach(({ actions, expected }, index) => {
        const game = new TennisGame1Refacto("player1", "player2");
        actions.forEach(player => game.wonPoint(player));
        const score = game.getScore();
        resultsRefacto.push({ scenario: index + 1, actions, score, expected });
    });

    const outputPathRefacto = path.join(__dirname, 'TennisGame1GoldenMasterResultsRefacto.json');
    fs.writeFileSync(outputPathRefacto, JSON.stringify(resultsRefacto, null, 2), 'utf-8');

    // Test to validate the creation of the original results file
    test('Golden Master results file for original should be created and valid', () => {
        expect(fs.existsSync(outputPathOriginal)).toBe(true);

        const fileContent = fs.readFileSync(outputPathOriginal, 'utf-8');
        const jsonContent = JSON.parse(fileContent);

        expect(jsonContent).toEqual(resultsOriginal);
    });

    // Test to validate the creation of the refactored results file
    test('Golden Master results file for refactored should be created and valid', () => {
        expect(fs.existsSync(outputPathRefacto)).toBe(true);

        const fileContent = fs.readFileSync(outputPathRefacto, 'utf-8');
        const jsonContent = JSON.parse(fileContent);

        expect(jsonContent).toEqual(resultsRefacto);
    });

    // Test to compare the two JSON files
    test('Comparison of original and refactored results', () => {
        const originalFileContent = fs.readFileSync(outputPathOriginal, 'utf-8');
        const refactoFileContent = fs.readFileSync(outputPathRefacto, 'utf-8');

        const originalJson = JSON.parse(originalFileContent);
        const refactoJson = JSON.parse(refactoFileContent);

        expect(originalJson).toEqual(refactoJson);
    });
});
