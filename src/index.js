const TennisGame1 = require('./TennisGame1');

// Initialisation des joueurs et de la langue
const player1Name = "player1";
const player2Name = "player2";

const actions = ["player1", "player1", "player2", "player2", "player1", "player2", "player1", "player2", "player1", "player1"];

// Test en anglais
const game1 = new TennisGame1(player1Name, player2Name, "en");

actions.forEach(player => game1.wonPoint(player));
console.log("=================");
console.log("Partie en anglais :");
console.log(`Score final: ${game1.getScore()}`);


// Test en français
const game2 = new TennisGame1(player1Name, player2Name, "fr");

actions.forEach(player => game2.wonPoint(player));
console.log("=================");
console.log("Partie en français :");
console.log(`Score final: ${game2.getScore()}`);
