const translations = require('../translations/translations.json');

class TennisGame1 {
    constructor(player1Name, player2Name, lang = 'en') {
        this.score1 = 0;
        this.score2 = 0;
        this.player1Name = player1Name;
        this.player2Name = player2Name;
        this.lang = translations[lang] ? lang : 'en';
    }

    wonPoint(playerName) {
        if (playerName === "player1") {
            this.score1 += 1;
        } else {
            this.score2 += 1;
        }
    }

    getScore() {
        if (this.score1 === this.score2) {
            return this.getEvenScore();
        } else if (this.score1 >= 4 || this.score2 >= 4) {
            return this.getEndGameScore();
        } else {
            return this.getRegularScore();
        }
    }

    getEvenScore() {
        const translation = translations[this.lang];
        switch (this.score1) {
            case 0:
                return translation["Love-All"];
            case 1:
                return translation["Fifteen-All"];
            case 2:
                return translation["Thirty-All"];
            default:
                return translation["Deuce"];
        }
    }

    getEndGameScore() {
        const translation = translations[this.lang];
        const scoreDifference = this.score1 - this.score2;
        if (scoreDifference === 1) {
            return translation["Advantage player1"];
        } else if (scoreDifference === -1) {
            return translation["Advantage player2"];
        } else if (scoreDifference >= 2) {
            return translation["Win for player1"];
        } else {
            return translation["Win for player2"];
        }
    }

    getRegularScore() {
        const translation = translations[this.lang];
        const scoreNames = [translation["Love"], translation["Fifteen"], translation["Thirty"], translation["Forty"]];
        return `${scoreNames[this.score1]}-${scoreNames[this.score2]}`;
    }
}

if (typeof window === "undefined") {
    module.exports = TennisGame1;
}
